import { render, fireEvent } from '@testing-library/svelte';

import EditorCueTime from './EditorCueTime';

describe('<EditorCueTime>', () => {
	beforeEach(() => {
		document.body.innerHTML = '';
	});
	it('should render', () => {
		const instance = render(EditorCueTime);
		expect(instance.getByTestId('timeSnapshot')).toBeValid();
		expect(instance.getByTestId('timeInput')).toBeValid();
	});
	it('should render time', () => {
		const instance = render(EditorCueTime, { seconds: 42 });
		const input = instance.getByTestId('timeInput');

		expect(input.value).toBe('42');
	});
	it('should render message', () => {
		const instance = render(EditorCueTime, { message: 'start⏱' });
		const button = instance.getByTestId('timeSnapshot');

		expect(button.value).toBe('start⏱');
	});
	it('should disable inputs', () => {
		const instance = render(EditorCueTime, { disabled: true });
		const button = instance.getByTestId('timeSnapshot');
		const input = instance.getByTestId('timeInput');

		expect(button.disabled).toBe(true);
		expect(input.disabled).toBe(true);
	});
	describe('#timeSnapshot', () => {
		it('should dispatch current time when clicked', async () => {
			const instance = render(EditorCueTime, { currentTime: 24, seconds: 42 });
			const button = instance.getByTestId('timeSnapshot');
			const mockSetTime = jest.fn();

			instance.component.$on('settime', mockSetTime);

			await fireEvent.click(button);

			expect(instance.component.seconds).toBe(24);
			expect (mockSetTime).toHaveBeenCalledTimes(1);
			expect (mockSetTime).toHaveBeenCalledWith(expect.objectContaining({ detail: 24 }));
		});
	});
	describe('#timeInput', () => {
		it('should dispatch seconds when inputted', async () => {
			const instance = render(EditorCueTime, { currentTime: 24, seconds: 42 });
			const input = instance.getByTestId('timeInput');
			const mockSetTime = jest.fn();

			instance.component.$on('settime', mockSetTime);

			await fireEvent.input(input, { target: { value: 43 } });

			expect(instance.component.seconds).toBe(43);
			expect (mockSetTime).toHaveBeenCalledTimes(1);
			expect (mockSetTime).toHaveBeenCalledWith(expect.objectContaining({ detail: 43 }));
		});
	});
});