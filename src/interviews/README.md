# AudioTranscript

A personal project building an audio player and interactive transcript with Svelte.

// screenshot

// animated gif of audio playing


## Features

- Small footprint // todo: size
- Supports multiple audio source files
- Interactive transcript support
	- emphasize active cues
	- jump the audio to a specific cue
- Editor mode to transcribe new audio recordings
    - fast mode:
        - tab on last cue to add new cues
        - Ctrl+Space to pause audio
        - disables other controls for focus on cue text
    - supports multiline cues
    - adjustable playback rate
    - capture start and end times
    - emphasized active cues to check timing
    - save generated WebVTT content


## Get started

Hosted at: https://sharp-davinci-8dd914.netlify.app/build/interviews

```html
<link rel="stylesheet" href="https://sharp-davinci-8dd914.netlify.app/build/interviews/bundle.css">
<script defer type="module" src="https://sharp-davinci-8dd914.netlify.app/build/interviews/bundle.js"></script>

<body>
	...
    <style>
        #demo {
            --transcript-highlight-color: aliceblue;
            --transcript-link-color: blue;
        }
    </style>
	<script type="module">
		import { AudioTranscript } from 'sharp-davinci-8dd914.netlify.app/build/interviews/bundle.js';

		const app = new AudioTranscript({
			target: document.getElementById('demo'),
			props: {
				sources: [
					{
						src: '/demo/how-to-eat-eggs.webm',
						type: 'audio/webm',
					},
					{
						src: '/demo/how-to-eat-eggs.mp4',
						type: 'audio/mp4',
					},
				],
				transcript: {
					src: '/demo/en_how-to-eat-eggs.vtt',
					lang: 'en',
				},
				editorMode: true,
			},
			slots: {}
		});
	</script>
</body>
```


### WebVTT captions

Cues require a unique identifer, for example:

```vtt
WEBVTT

NOTE This is cue id 1
1
00:00:01.000 --> 00:00:04.000
Starting interview here with Theo and Eva.

NOTE This is cue id 2
2
00:00:04.000 --> 00:00:08.000
Can you tell me what kind of egg that you like to eat?

3
00:00:08.000 --> 00:00:12.000
T: Uh, all of the eggs.
...
```


## Running the demo

Install the dependencies...

```bash
cd sarahgrefalda-components
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see the demo app running.


## Architecture

Not an authoritative source but a working draft of the component architecture:

```mermaid
classDiagram
    AudioTranscript --> AudioTranscriptPlayer
    AudioTranscript --> AudioTranscriptEditor
    AudioTranscript --> AudioControls
    AudioTranscriptPlayer --> PlayerCue
    AudioTranscriptEditor --> EditorCue
    AudioTranscriptEditor --> EditorControls
    AudioTranscriptEditor --> EditorCueTime
    PlayerCue --> JumpToCue
    EditorCue --> JumpToCue

    AudioTranscript : MediaSources sources
    AudioTranscript : Transcript transcript
    AudioTranscript : boolean editorMode
    AudioTranscriptPlayer : int duration
    AudioTranscriptPlayer : HTMLTextCues activeCues
    AudioTranscriptPlayer : HTMLTextCues cues
    AudioTranscriptPlayer : jumpto()
    AudioTranscriptEditor : int currentTime
    AudioTranscriptEditor : int duration
    AudioTranscriptEditor : HTMLTextCues activeCues
    AudioTranscriptEditor : HTMLTextCues cues
    AudioTranscriptEditor : jumpto()
    AudioTranscriptEditor : addCue()
    AudioTranscriptEditor : updateCue()
    AudioControls : int currentTime
    AudioControls : int duration
    AudioControls : int volume
    AudioControls : play()
    AudioControls : pause()
    AudioControls : jumpto()
    AudioControls : setVolume()
    PlayerCue : HTMLTextCue cue
    PlayerCue : jumpto()
    JumpToCue : int time
    JumpToCue : jumpto()
    EditorCue : int startTime
    EditorCue : int endTime
    EditorCue : string simpleText
    EditorCue : jumpto()
    EditorCue : setText()
    EditorCue : setStartTime()
    EditorCue : setEndTime()
    EditorControls : int playbackRate
    EditorControls : setPlaybackRate()
    EditorCueTime : int time
    EditorCueTime : setTime()
```


## Attribution

Generated from a Svelte project template: https://github.com/sveltejs/template. To create a new project based on this template using [degit](https://github.com/Rich-Harris/degit):

```bash
npx degit sveltejs/template svelte-app
cd svelte-app
```

*Note that you will need to have [Node.js](https://nodejs.org) installed.*

