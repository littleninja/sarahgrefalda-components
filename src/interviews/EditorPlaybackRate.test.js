import { render } from '@testing-library/svelte';
import userEvent from '@testing-library/user-event';

import EditorPlaybackRate from './EditorPlaybackRate';

describe('<EditorPlaybackRate>', () => {
	beforeEach(() => {
		document.body.innerHTML = '';
	});
	it('should render', () => {
		const instance = render(EditorPlaybackRate);
		const select = instance.getByTestId('playbackRate');
		const options = instance.getAllByTestId('playbackRateOption');
		expect(select).toBeValid();
		expect(options[0].selected).toBe(true);
		expect(instance.container.textContent).toMatch('Playback rate');
	});
	it('should dispatch playback at full speed (1.0)', () => {
		const instance = render(EditorPlaybackRate);
		const select = instance.getByTestId('playbackRate');
		const options = instance.getAllByTestId('playbackRateOption');
		const mockPlaybackRate = jest.fn();
		instance.component.$on('playbackrate', mockPlaybackRate);

		userEvent.selectOptions(select, ['1.00']);

		expect(options[0].selected).toBe(true);
		expect(mockPlaybackRate).toHaveBeenCalledTimes(1);
		expect(mockPlaybackRate).toHaveBeenCalledWith(expect.objectContaining({ detail: 1.0 }));
	});
	it('should dispatch playback at three-quarter speed (0.75)', () => {
		const instance = render(EditorPlaybackRate);
		const select = instance.getByTestId('playbackRate');
		const options = instance.getAllByTestId('playbackRateOption');
		const mockPlaybackRate = jest.fn();
		instance.component.$on('playbackrate', mockPlaybackRate);

		userEvent.selectOptions(select, ['0.75']);

		expect(options[1].selected).toBe(true);
		expect(mockPlaybackRate).toHaveBeenCalledTimes(1);
		expect(mockPlaybackRate).toHaveBeenCalledWith(expect.objectContaining({ detail: 0.75 }));
	});
	it('should dispatch playback at half speed (0.5)', () => {
		const instance = render(EditorPlaybackRate);
		const select = instance.getByTestId('playbackRate');
		const options = instance.getAllByTestId('playbackRateOption');
		const mockPlaybackRate = jest.fn();
		instance.component.$on('playbackrate', mockPlaybackRate);

		userEvent.selectOptions(select, ['0.50']);

		expect(options[2].selected).toBe(true);
		expect(mockPlaybackRate).toHaveBeenCalledTimes(1);
		expect(mockPlaybackRate).toHaveBeenCalledWith(expect.objectContaining({ detail: 0.5 }));
	});
	it('should dispatch playback at quarter speed (0.25)', () => {
		const instance = render(EditorPlaybackRate);
		const select = instance.getByTestId('playbackRate');
		const options = instance.getAllByTestId('playbackRateOption');
		const mockPlaybackRate = jest.fn();
		instance.component.$on('playbackrate', mockPlaybackRate);

		userEvent.selectOptions(select, ['0.25']);

		expect(options[3].selected).toBe(true);
		expect(mockPlaybackRate).toHaveBeenCalledTimes(1);
		expect(mockPlaybackRate).toHaveBeenCalledWith(expect.objectContaining({ detail: 0.25 }));
	});
});