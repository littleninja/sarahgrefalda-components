import { render, fireEvent } from '@testing-library/svelte';

import JumpToCue from './JumpToCue';

describe('<JumpToCue>', () => {
	beforeEach(() => {
	document.body.innerHTML = '';
	});
	it('should render', () => {
		const instance = render(JumpToCue);
		expect(instance.getByTestId('jump')).toBeValid();
	});
	it('should render formatted time', () => {
		const instance = render(JumpToCue, { startTime: 12, duration: 42 });
		const jump = instance.getByTestId('jump');
		expect(jump.textContent).toBe('00:12');
	});
	it('should disable', () => {
		const instance = render(JumpToCue, { startTime: 12, duration: 42, disabled: true });
		const jump = instance.getByTestId('jump');
		expect(jump.disabled).toBe(true);
	});
	it('should dispatch jump to start time when clicked', async () => {
		const instance = render(JumpToCue, { startTime: 12, duration: 42 });
		const jump = instance.getByTestId('jump');
		const mockJump = jest.fn();
		instance.component.$on('jumpto', mockJump);

		await fireEvent.click(jump);

		expect(mockJump).toHaveBeenCalledTimes(1);
		expect(mockJump).toHaveBeenCalledWith(expect.objectContaining({ detail: 12 }))
	});
});