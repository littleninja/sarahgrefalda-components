import { formatVttTime } from './maths';

describe('formatVttTime()', () => {
	it('should format milleseconds', () => {
		expect(formatVttTime(0.000)).toBe('00:00:00.000');
		expect(formatVttTime(0.001)).toBe('00:00:00.001');
		expect(formatVttTime(0.021)).toBe('00:00:00.021');
		expect(formatVttTime(0.321)).toBe('00:00:00.321');
		expect(formatVttTime(0.320)).toBe('00:00:00.320');
		expect(formatVttTime(0.300)).toBe('00:00:00.300');
	});
	it('should format seconds', () => {
		expect(formatVttTime(1.002)).toBe('00:00:01.002');
		expect(formatVttTime(12.345)).toBe('00:00:12.345');
		expect(formatVttTime(59.999)).toBe('00:00:59.999');
	});
	it('should format minutes', () => {
		expect(formatVttTime(62.003)).toBe('00:01:02.003');
		expect(formatVttTime(601.456)).toBe('00:10:01.456');
		expect(formatVttTime(3599.999)).toBe('00:59:59.999');
	});
	it('should format hours', () => {
		expect(formatVttTime(3601.001)).toBe('01:00:01.001');
		expect(formatVttTime(3611.000)).toBe('01:00:11.000');
		expect(formatVttTime(3661.000)).toBe('01:01:01.000');
		expect(formatVttTime(4261.000)).toBe('01:11:01.000');
		expect(formatVttTime(39661.000)).toBe('11:01:01.000');
		expect(formatVttTime(40261.000)).toBe('11:11:01.000');
		expect(formatVttTime(40271.000)).toBe('11:11:11.000');
	});
});