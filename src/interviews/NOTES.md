# Project notes

These are some notes to self.

## Ideas

### Linear gradient background animations

We can't animate the properties of a linear gradient, for example interpolating the stop points, but we could interpolate the background-position-y and other attributes. 


### Subtitle support

Collaborate with listeners in other languages to translate the captions into other languages and offer subtitle support. [Use `navigator.languages`](https://stackoverflow.com/questions/1043339/javascript-for-detecting-browser-language-preference) to detect user's preferred languages.

Assumption: subtitles require the exact ids as the cues to display and emphasize while playing. Could there be an optional choice to include original captions for listeners/readers hoping to learn the original language?

A form control would allow the user pick a language.


### Sign-language video support

Another way to make audio-only content more inclusive is to offer a video in sign langauge. I love this idea, here are additional thoughts:

- Finding someone to translate into American sign language
- Include multilingual sign language support when adding subtitles

