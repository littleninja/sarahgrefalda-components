export function formatVttTime(seconds) {
	const { ms, min, sec, hr } = getTimeUnits(seconds);
	return leftPad(hr) + ':' + leftPad(min) + ':' + leftPad(sec) + ms;	
}

/**
 * Given the duration or maximum possible time, formats the current
 * time to omit units that would always be zero (e.g. hours).
 * Does not include milleseconds.
 */
export function formatTimestamp(currentTimeSeconds, durationSeconds) {
	if (!durationSeconds) {
		return;
	}
	let includeHours = parseInt(durationSeconds,10) > 60 * 60;
	const { sec, min, hr } = getTimeUnits(currentTimeSeconds);
	return (includeHours ? leftPad(hr) + ':' : '') + leftPad(min) + ':' + leftPad(sec);
}

function getTimeUnits(seconds) {
	let base = parseInt(seconds, 10);
	const ms = (seconds - base).toFixed(3).toString().slice(1);
	const hr = Math.floor(base / 3600);
	const min = Math.floor(base / 60) - hr * 60;
	const sec = base - hr * 3600 - min * 60;
	return {
		ms,
		sec,
		min,
		hr
	};
}

function leftPad(number) {
	return (number < 10 ? '0' : '') + number;
}