import { render } from '@testing-library/svelte';

import AudioTranscript from './AudioTranscript';

describe('<AudioTranscript>', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
  });
  it('should handle defaults (1)', () => {
    expect(render(AudioTranscript).getByTestId('audio')).toBeValid();
  });
  it('should handle defaults (2)', () => {
    expect(render(AudioTranscript, {}).getByTestId('audio')).toBeValid();
  });
  it('should load media sources', () => {
    const player = render(AudioTranscript, {
      sources: [
        {
          src: 'important-talk.webm',
          type: 'audio/webm'
        },
        {
          src: 'important-talk.mp4',
          type: 'audio/mp4'
        },
      ]
    });
    const audio = player.getByTestId('audio');
    expect(audio).toBeValid();
    expect(audio).toContainHTML('<source src="important-talk.webm" type="audio/webm">');
    expect(audio).toContainHTML('<source src="important-talk.mp4" type="audio/mp4">');
  });
});